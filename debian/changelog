php-oauth (2.0.9-2) unstable; urgency=medium

  * Add compatibility shim for PHP 7.0

 -- Ondřej Surý <ondrej@debian.org>  Tue, 04 Mar 2025 20:53:12 +0100

php-oauth (2.0.9-1) unstable; urgency=medium

  * Fix Build-Depend on php-all-dev (>= 2:95~)
  * New upstream version 2.0.9

 -- Ondřej Surý <ondrej@debian.org>  Tue, 26 Nov 2024 06:20:00 +0100

php-oauth (2.0.7++-4) unstable; urgency=medium

  * Upload to unstable

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 19:47:00 +0200

php-oauth (2.0.7++-4~exp7) experimental; urgency=medium

  * Rebuild with PHP 8.4.0RC1

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 16:14:58 +0200

php-oauth (2.0.7++-4~exp6) experimental; urgency=medium

  * Use upstream patch to fix PHP 8.4 FTBFS

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 15:49:22 +0200

php-oauth (2.0.7++-4~exp5) experimental; urgency=medium

  * Fix more FTBFS with PHP 8.4, the oauth extensions hasn't
    upgrade its internals for a long time.

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 14:21:25 +0200

php-oauth (2.0.7++-4~exp4) experimental; urgency=medium

  * Replace php_rand() with php_mt_rand() to fix FTBFS with PHP 8.4
    and don't include php_rand.h and php_lcg.h

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 11:33:39 +0200

php-oauth (2.0.7++-4~exp3) experimental; urgency=medium

  * Fix Build-Depend on php-all-dev (>= 2:95~)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 24 Sep 2024 14:49:40 +0200

php-oauth (2.0.7++-4~exp2) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:39 +0200

php-oauth (2.0.7++-4~exp1) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:36 +0200

php-oauth (2.0.7++-4) unstable; urgency=medium

  * Bump Build-Depends to dh-php >= 5.5~

 -- Ondřej Surý <ondrej@debian.org>  Sun, 22 Sep 2024 08:12:46 +0200

php-oauth (2.0.7++-3) unstable; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Sep 2024 12:57:28 +0200

php-oauth (2.0.7++-2) unstable; urgency=medium

  * Update changelog for 2.0.7++-1 release
  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:33:55 +0200

php-oauth (2.0.7++-1) unstable; urgency=medium

  * New upstream version 2.0.7++
  * Finish splitting the source package for PHP 7.x and PHP 8.x
  * Rebuild on a system with newer glibc

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 14:46:10 +0200

php-oauth (2.0.7+1.2.3-16) unstable; urgency=medium

  * Don't fail on failed tests - the tests are still too fragile to cause
    hard fail

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 12:01:27 +0100

php-oauth (2.0.7+1.2.3-15) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:56:46 +0100

php-oauth (2.0.7+1.2.3-14) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:29 +0100

php-oauth (2.0.7+1.2.3-13) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:52 +0100

php-oauth (2.0.7+1.2.3-11) unstable; urgency=medium

  * Remove libpcre3-dev build depends (Closes: #999996)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 28 Nov 2021 09:20:43 +0100

php-oauth (2.0.7+1.2.3-10) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:26:23 +0100

php-oauth (2.0.7+1.2.3-9) unstable; urgency=medium

  * Remove dummy pcre.h config check (it actually needs php_pcre.h, not
    pcre.h) (Closes: #999996)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 18 Nov 2021 13:26:23 +0100

php-oauth (2.0.7+1.2.3-8) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:00 +0100

php-oauth (2.0.7+1.2.3-7) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:07:31 +0100

php-oauth (2.0.7+1.2.3-6) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:39 +0100

php-oauth (2.0.7+1.2.3-5) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:06 +0100

php-oauth (2.0.7+1.2.3-4) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:55 +0100

php-oauth (2.0.7+1.2.3-3) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:40:31 +0100

php-oauth (2.0.7+1.2.3-2) unstable; urgency=medium

  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 14:45:33 +0200

php-oauth (2.0.7+1.2.3-1) unstable; urgency=medium

  * New upstream version 2.0.7+1.2.3
  * Build with dh-php >= 1.0

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Oct 2020 22:11:05 +0200

php-oauth (2.0.5+1.2.3-1) unstable; urgency=medium

  * New upstream version 2.0.5+1.2.3

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Mar 2020 08:47:42 +0100

php-oauth (2.0.4+1.2.3-1) unstable; urgency=medium

  * New upstream version 2.0.4+1.2.3

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:23:02 +0100

php-oauth (2.0.3+1.2.3-3) unstable; urgency=medium

  * Add patch to support PHP 7.4 (Courtesy of Remi Collet)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 29 Oct 2019 16:21:46 +0100

php-oauth (2.0.3+1.2.3-2) unstable; urgency=medium

  * No change rebuild for Debian buster

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Aug 2019 09:20:37 +0200

php-oauth (2.0.3+1.2.3-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 2.0.3+1.2.3

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 08:50:57 +0200

php-oauth (2.0.2+1.2.3-3) unstable; urgency=medium

  * Bump the dependency on dh-php to >= 0.33

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 10:21:04 +0000

php-oauth (2.0.2+1.2.3-2) unstable; urgency=medium

  * Update Vcs-* to salsa.d.o
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899644)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 12:53:40 +0000

php-oauth (2.0.2+1.2.3-1) unstable; urgency=medium

  * Imported Upstream version 2.0.2+1.2.3

 -- Ondřej Surý <ondrej@debian.org>  Mon, 07 Nov 2016 10:16:25 +0100

php-oauth (2.0.1+1.2.3+-2) unstable; urgency=medium

  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 11:19:01 +0200

php-oauth (2.0.1+1.2.3+-1) unstable; urgency=medium

  * Imported Upstream version 2.0.1+1.2.3+
  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 16:05:49 +0200

php-oauth (2.0.1+1.2.3-1) unstable; urgency=medium

  [ Prach Pongpanich ]
  * Fix debian/watch to correct package name
  * Bump standards version to 3.9.5

  [ Ondřej Surý ]
  * Update gbp.conf for new branches
  * Bump standards version to 3.9.7
  * Imported Upstream version 2.0.1+1.2.3
  * Update packaging for dual-versions for PHP 5.6 and PHP 7.0

 -- Ondřej Surý <ondrej@debian.org>  Mon, 28 Mar 2016 22:23:50 +0200

php-oauth (1.2.3-1) unstable; urgency=low

  [ Prach Pongpanich ]
  * Initial release. (Closes: #668315)
  * Update a gbp.conf file

  [ Ondřej Surý ]
  * Use dh_php5 for generating Depends
  * Update debian/copyright to match the package
  * Use pristine-tar
  * Update debian/watch to match to package
  * Add myself to uploaders
  * The main package should be php5-oauth and not php-oauth

 -- Ondřej Surý <ondrej@debian.org>  Mon, 21 Oct 2013 11:04:36 +0200
